<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\AddressConnection;
use App\Models\AddressIgnore;
use App\Models\AddressPerson;
use App\Models\AddressTag;
use App\Models\AddressProduct;
use App\Models\BadenCity;
use App\Models\Cluster;
use App\Models\CustomerType;
use App\Models\Favorite;
use App\Models\People;
use App\Models\Product;
use App\Models\SwitzerlandCanton;
use App\Models\Tag;
use App\Models\Tender;
use App\Models\User;
use App\Models\UserEdit;
//use App\Services\AddressesService;
use App\Services\AddresseService;
use App\Services\GlobalHelper;
use App\Services\GlobalSearchService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Image;
use File;
use LabscapeRoche\LabscapeRocheService;
use stdClass;
use Storage;
use Tymon\JWTAuth\Facades\JWTAuth;

class AddressesController extends Controller
{

    public $addressIdsDefinedWhenIteration = [];
    /** @var AddressesService  */
    public $addressesService;

    public function __construct(AddressesService $addressesService)
    {
        parent::__construct();
        $this->addressesService = $addressesService;
    }


    function index(AddressIndexRequest $request)
    {
        $addressesForResponse = $this->addressesService->fetchAddressesForUsaUser($request);
        return response()->json($addressesForResponse);
    }


    function loadAddressesPaginated(AddressesService $addressesService)
    {
        $addresses = $addressesService->loadAddressesPaginated();
        return response()->json($addresses);

//        $addresses = $this->composePaginatedResponseViaPureSql();
//        $this->addTagsToAddresses($addresses['data']);
//        $this->addFavoriteStatusToAddresses($addresses['data']);
//        if(Auth::user()->default_country === 'usa-private') {
//            $this->addGrowthOpp($addresses['data']);
//        }
//        return response()->json($addresses);
    }


    function addTagsToAddresses($addresses)
    {
        foreach ($addresses as $i => $address) {
            $sql = "SELECT GROUP_CONCAT(t2.name SEPARATOR ', ') as tags_str
                            FROM `rl_tags` as t2
                            LEFT JOIN rl_address_tags AS at2
                            ON at2.tag_id = t2.id
                            WHERE at2.address_id = $address->id
                            GROUP BY at2.address_id
                      ";

            $tag = $this->DBWithConnection()->select($sql);

            $tagsStr = !empty($tag) ? $tag[0]->tags_str : '';

            $addresses[$i]->tags_str = $tagsStr;
        };
    }


    private function addGrowthOpp($addresses)
    {
        foreach ($addresses as $i => $address) {
            $sql = "SELECT sv1.scalar_value as growthopp 
                    FROM rl_addresses as a
                    LEFT JOIN rsm_scalar_values AS sv1 
                      ON sv1.rsm_id = a.RSMf_SiteMasterId 
                      AND sv1.franchise = 'Enterprise' 
                      AND sv1.scalar_type_id = 10
                    WHERE a.id = $address->id
                    ";

            $res = $this->DBWithConnection()->select($sql);

            $growthOpp = !empty($res) ? $res[0]->growthopp : '';

            $addresses[$i]->growthopp = intval($growthOpp);
        }
    }


    function addFavoriteStatusToAddresses ($addresses)
    {
        foreach ($addresses as $i => $address) {
            $address->is_favorite = Favorite::isFavorite(Auth::user()->id, 'ADDRESS', $address->id);
        };
    }

    /**
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function preProcessGlobalSearch(SearchRequest $request)
    {
        $searchStr = $request->get('global-search');
        $addressesCount = $this->addressesService->prepareAddressesQuery($request)->count();
        $peopleCount = People::where('name', 'like', '%' . $searchStr . '%')->count();

        return response()->json([
            'count_addresses' => $addressesCount,
            'count_people' => $peopleCount
        ], JsonResponse::HTTP_OK);
    }

    function getAddressIdsForIteration($requestParams)
    {
        $GSS = new GlobalSearchService();
        $groupedSearchIterations = $GSS->groupSearchIterationsByEntity($requestParams['iteration']);

        $isWithLevenstein = isset($requestParams['with-levenstein']);

        $addressIds = $GSS->searchForAddressesIds($groupedSearchIterations, $isWithLevenstein, GlobalHelper::defineIsAddressesOrClustersShouldBeOrdered());

        return $addressIds;
    }


    function loadFilterValues()
    {
        if(Auth::user()->default_country === 'usa-private') {
            $rocheService = new LabscapeRocheService($this);
            $tags = $rocheService->getFilterTagValues();
            $relationalTags = [];

            $products = $rocheService->getProductFilterValues();
            $relationalProducts = $rocheService->getRelationalProductFilterValues();

            $sortByOptions = $rocheService->getSortByFilterOptions();
        }
        else {
            $tags = Tag::get(['id', 'name']);

            $relationalTags = collect($this->getRelationalTagsParent());

            $relationalTags->each(function ($tag) {
                $tag->childProducts = Tag::where('parent_tag_id', $tag->id)
                    ->orderBy('name')
                    ->groupBy('rl_tags.id')
                    ->get(['id', 'name']);
            });

            $products = Product::orderByRaw('company, name')->get();
            $relationalProducts = Product::where(function ($q){
                $q->whereRaw("rl_products.name IS NULL OR rl_products.name = ''");
            })
                ->orderByRaw('company, name')
                ->get();

            $relationalProducts->each(function ($product) {
                $product->childProducts = Product::where('company', $product->company)
                    ->orderByRaw('company, name')
                    ->get();
            });

            $sortByOptions = $this->getSortByOptions();
        }

        $personTypes = $this->DBWithConnection()->table('rl_people_types')->get();

        $customerTypes = CustomerType::visible()->get();

        $filters = [
            'tag_list' => $tags,
            'used_product_list' => $products,
            'customer_types' => $customerTypes,
            'person_types' => $personTypes,
            'relational_products' => $relationalProducts,
            'relational_tags' => $relationalTags,
            'sort_by' => $sortByOptions
        ];

        if(GlobalHelper::isWealthscapeApp()) {

            $cantons = SwitzerlandCanton::all();

            $relationalCantons = SwitzerlandCanton::where('parent_id', 0)->get();
            $relationalCantons->each(function ($canton) {
                $canton->childProducts = SwitzerlandCanton::where('parent_id', $canton->id)
                    ->orderBy('name')
                    ->groupBy('rl_geo_hierarchy.id')
                    ->get(['id', 'name']);
            });

            $filters['location_list'] = $cantons;
            $filters['relational_locations'] = $relationalCantons;


            $filters['assignee_list'] = User::getUserListForAssigning();

            $filters['baden_cities'] = $this->composeBadenCitiesFilter();
            $filters['relational_baden_cities'] = $this->composeRelationslBadenCitiesFilter($filters['baden_cities']);

        }

        return response()->json($filters);
    }


    private function composeBadenCitiesFilter()
    {
        $cities = [];

        $badenCities = BadenCity::all();

        foreach ($badenCities as $k => $city) {
            $cities[$k]['id'] = $city->id;
            $cities[$k]['name'] = $city->city;
        }

        return $cities;
    }


    private function composeRelationslBadenCitiesFilter($badenCities)
    {
        $relationalCities = [];

        $relationalCities[] = [
            'id' => -2,
            'parent_tag_id' => 0,
            'name' => 'Others',
            'childProducts' => []
        ];

        $relationalCities[] = [
            'id' => -1,
            'parent_tag_id' => 0,
            'name' => 'Bezirk Baden',
            'childProducts' => $badenCities
        ];

        return $relationalCities;
    }


    function getSortByOptions()
    {
        $options = [
            ['value' => 'name-asc', 'label' => 'Name &uarr;'],
            ['value' => 'name-desc', 'label' => 'Name &darr;'],
            ['value' => 'people-asc', 'label' => 'Employee &uarr;'],
            ['value' => 'people-desc', 'label' => 'Employee &darr;'],
        ];

        if(env('APP_SCOPE') !== 'wealthscape') {
            array_push($options, ['value' => 'products-asc', 'label' => 'Products &uarr;']);
            array_push($options, ['value' => 'products-desc', 'label' => 'Products &darr;']);
        }

        return $options;
    }


    function showSimpleDetails($id)
    {
        $address = Address::find($id);

        return response()->json($address);
    }

    function getRelationalTagsParent ()
    {
        return $this->addressesService->getResultFromCache("
                                SELECT t1.*
                        FROM rl_tags as t1
                        WHERE t1.parent_tag_id IS NULL OR t1.parent_tag_id = 0
                        GROUP BY t1.id
                        
                        UNION
                        
                        SELECT t2.*
                        FROM rl_tags as t2
                        JOIN rl_tags as t3
                          ON t3.parent_tag_id = t2.id
                        WHERE t2.parent_tag_id IS NULL OR t2.parent_tag_id = 0
                        GROUP BY t2.id
                        
                        ORDER BY name
                    ");
    }


    function show($address)
    {
        $address = Address::find($address);

        if(Auth::user()->default_country === 'usa-private') {
            $address->tags = LabscapeRocheService::getAddressTagsAsArrayOfObjects($address->RSMf_SiteMasterId, $this->DBWithConnection());
        }
        else {
            $address->load('tags');
        }

        $address->load('cluster');

        $address->load(['cluster.addresses' => function($q){
            $q->take(4);
        }]);

        $address->load(['people' => function($query){
            $query->withCount('relationships');
            $query->groupBy('name');
            $query->orderBy('relationships_count', 'DESC');
            $query->take(4);
        }]);
        $address->load([
            'products' => function ($query) {
                $query->orderByRaw('company, name');
            }
        ]);
	    $address->load('tenders.purchase');

	    if(!empty($address->cluster)) {
            $address->cluster->addresses_count = $address->cluster->getAddressMembersNumber();
        }

        if(Auth::user()->default_country == 'usa-private') {
            $this->addUsaPrivateSpecificFeatures($address);
        }

        $address->is_in_ignore_list = AddressIgnore::checkIfAddressIgnored($address->id);

	    $address = $address->toArray();

	    $address['tenders'] = Tender::where('address_id', $address['id'])->threeProductsWithMostBudgetSpent()->get();

	    $address['has_relationships_for_graph'] = Address::hasRelationshipsForGraph($address['id']);

	    $address['is_favorite'] = Favorite::isFavorite(Auth::user()->id, 'ADDRESS', $address['id']);

        return response()->json($address);
    }


    function addUsaPrivateSpecificFeatures(Address $address)
    {
        $address->awards = $this->getAddressAwards($address->id);

        $address->rms_ext = Address::getUsRmsExtValues($address->RSMf_SiteMasterId);

        if(!empty($address->cluster->addresses)){
            $address->cluster->addresses->each(function($item, $k) {
                $item->load(['scalar_values' => function($q){
                    $q->whereRaw('(scalar_type_id = 1 OR scalar_type_id= 10)');
                }]);
            });
        }

        $lrs = new LabscapeRocheService($this);

        $address->market_programs = $lrs->getMarketProgramsByRsmIdAndSiteLvl($address->RSMf_SiteMasterId, 'Site');
    }


    function updateCustomerStatus($address)
    {
        $address = Address::find($address);

        $data = request()->validate([
            'status' => 'required|integer',
        ]);

        $address->customer_status = $data['status'];
        $address->save();

        return response()->json($address);
    }


    function getSimpelListOfEmployees ($addressId)
    {
        $sql = "SELECT p.id, p.name, IFNULL(ap.role, p.role) as role 
                FROM rl_address_people as ap
                JOIN rl_people as p 
                  ON p.id = ap.person_id
                WHERE ap.address_id = $addressId
                ";

        $employees = $this->DBWithConnection()->select($sql);

        return response()->json($employees);
    }

    function loadPeopleByAddressId ($address)
    {
        $params = request()->all();

        if(isset($params['simple-list'])) {
            return $this->getSimpelListOfEmployees($address);
        }

        $address = Address::find($address);

        if(isset($params['main-person-id']) && !$this->isPersonAnEmployee($address, $params['main-person-id'])) {
            return ['data' => [], 'total' => 0];
        }

        $people = People::with('addresses')
                        ->with(['addressPerson' => function($q) use($address) {
                            $q->where('address_id', $address->id);
                        }])
                        ->withCount('relationships')
                        ->where(function ($q) use ($params) {

                            if(isset($params['name'])) {
                                $q->where('name', 'like', "%$params[name]%");
                            }
                        })
                        ->whereHas('addressPerson', function($q) use ($params){
                            if(isset($params['role'])) {

                                $role = $params['role'];

                                if($role === '--empty--') {
                                    $role = "";
                                }

                                $q->where('role', $role);
                            }
                        })
                        ->whereHas('addresses', function ($q) use ($address){
                            return $q->where('id', $address->id);
                        })
                        ->groupBy('rl_people.name')
                        ->orderBy('relationships_count', 'DESC')
                        ->paginate(10);

        return response()->json($people);
    }


    function isPersonAnEmployee($address, $personId)
    {
        $sql = "SELECT * FROM rl_address_people WHERE address_id = $address->id AND person_id = $personId";

        $result = $this->DBWithConnection()->select($sql);

        return !empty($result);
    }


    function getContactsChain($address)
    {

        $address = Address::find($address);

        $mainLabId = $address->id;

        $sqlQuery = "SELECT a2.id, a2.name, a2.cluster_id FROM rl_addresses a JOIN rl_addresses a2 WHERE (a.cluster_id = a2.cluster_id OR a2.id = ? ) AND a.id = ?";

        $cluster_labs = array_pluck($this->DBWithConnection()->select(DB::raw($sqlQuery), [$mainLabId, $mainLabId]), 'id');

        $cluster_labs_ids = implode(',', $cluster_labs);

        $sql = "SELECT * from
                (SELECT a.id, a.name, a.cluster_id, a.address FROM rl_addresses a WHERE a.id IN (" . $cluster_labs_ids . ") 
                UNION
                SELECT a2.id, a2.name, a2.cluster_id, a2.address FROM rl_addresses a  
                JOIN rl_address_people ap ON a.id = ap.address_id -- workers of main hospital
                JOIN rl_address_connections ac ON ac.from_person_id = ap.person_id -- people who know people on main hospital
                JOIN rl_address_people ap2 ON ap2.person_id = ac.to_person_id -- workplaces of people who know people on main hospital
                JOIN rl_addresses a2 ON ap2.address_id = a2.id 
                WHERE a.id IN (" . $cluster_labs_ids . ") 
                UNION 
                SELECT a2.id, a2.name, a2.cluster_id, a2.address FROM rl_addresses a  
                JOIN rl_address_people ap ON a.id = ap.address_id -- workers of main hospital
                JOIN rl_address_connections ac ON ac.to_person_id = ap.person_id -- people who know people on main hospital 
                JOIN rl_address_people ap2 ON ap2.person_id = ac.from_person_id -- workplaces of people who know people on main hospital
                JOIN rl_addresses a2 ON ap2.address_id = a2.id 
                WHERE a.id IN(" . $cluster_labs_ids . ")              
                ) related_labs ";

        $related_labs = $this->DBWithConnection()->select(DB::raw($sql));

        $related_labs_ids = "";
        $first = true;
        foreach ($related_labs as $lab){
            if ($first){
                $first = false;
            }else{
                $related_labs_ids = $related_labs_ids . ",";
            }
            $related_labs_ids = $related_labs_ids . $lab ->id;
        }

        $sql = "SELECT rl.id, rl.name, ap.address_id, pt.name as 'workerType' 
                FROM rl_people rl  
                JOIN rl_address_people ap ON ap.person_id = rl.id 
                JOIN rl_people_types pt ON rl.type_id = pt.id  
                WHERE ap.address_id IN (" . $related_labs_ids . ")";

        $lab_workers = $this->DBWithConnection()->select(DB::raw($sql));


        $related_people = [];
        if ($related_labs_ids != ""){
            $sql = "SELECT p.id, p.name, ap.address_id FROM rl_address_people ap JOIN rl_people p ON ap.person_id = p.id  
                    WHERE ap.address_id IN (" . $related_labs_ids . ")";

            $related_people = $this->DBWithConnection()->select(DB::raw($sql));
        }

        // get the relations from related people
        $first = true;
        $related_people_ids = "";
        foreach ($related_people as $p){
            if ($first){
                $first = false;
            }else{
                $related_people_ids = $related_people_ids . ",";
            }
            $related_people_ids = $related_people_ids . $p->id;
        }

        // get relationships and descriptions
        $people_relationships = [];
        if ($related_people_ids != ""){
            $sql = "SELECT ac.from_person_id, ac.to_person_id, ac.edge_weight, act.id as 'connection_type' 
              FROM rl_address_connections ac LEFT JOIN rl_address_connection_types act on ac.edge_type = act.id 
            WHERE ac.from_person_id IN (" . $related_people_ids . ") AND ac.to_person_id IN (" . $related_people_ids . ") ";

            $people_relationships = $this->DBWithConnection()->select(DB::raw($sql));
        }

        $result = [ 'related_labs' => $related_labs, 'related_people' => $related_people, 'relationships' => $people_relationships, 'workers' => $lab_workers ];

        return response()->json($result);
    }


    function getClusterMembersPaginated($address)
    {
        $address = Address::find($address);

        $search = request()->search;

        $clusterAddressesQuery = Address::where('cluster_id', $address->cluster_id);

        if(!empty($search)) {
            $clusterAddressesQuery = $clusterAddressesQuery->where('name', 'like', '%'.$search.'%');
        }

        if(Auth::user()->default_country === 'usa-private') {
            $clusterAddressesQuery->with('scalar_values');
        }

        if(Auth::user()->default_country === 'usa-private' && Auth::user()->isAclUser()) {
            $aclAddressIds = Auth::user()->getAddressIdsViaAclAddressRsmIds();

            $clusterAddressesQuery = $clusterAddressesQuery->whereIn('id', $aclAddressIds);
        }

        $clusterAddresses = $clusterAddressesQuery->paginate(10);

        return response()->json($clusterAddresses);
    }


    function getClusterStaffPaginated($address)
    {
        $address = Address::find($address);

        $query = People::with('addresses')
            ->whereHas('addresses', function ($q) use ($address) {
                $q->where('cluster_id', $address->cluster_id);
            });

        $query = $this->composeClusterStuffConditions($query, request()->all());

        $clusterStaff = $query->orderByRaw('name')
            ->paginate(10);

        return response()->json($clusterStaff);
    }


    function composeClusterStuffConditions($query, $params)
    {
        if (isset($params['name'])) {
            $query->where('rl_people.name', 'like', '%'.$params['name'].'%');
        }

        if (isset($params['type'])) {
            $query->where('rl_people.type_id', $params['type']);
        }

        return $query;
    }


    function getClusterProductsPaginated($address)
    {
        $address = Address::find($address);

        $products = Product::with([
                'addresses' => function ($query) use ($address) {
                    $query->where('cluster_id', $address->cluster_id);
                }
            ])
            ->whereHas('addresses', function ($q) use ($address) {
                $q->where('cluster_id', $address->cluster_id);
            })
            ->orderByRaw('company, name')
            ->paginate(10);

        return response()->json($products);
    }

    /**
     * update Address
     */
    public function updateAddressDetails($address)
    {
        $address = Address::find($address);

        $params = request()->only(['name', 'address', 'url', 'phone']);

        $params['updated_by'] = 'labscape';

        $address->update($params);

        $tags = request()->get('tags');

        $ids = [];

        foreach ($tags as $tag) {

            $tagName = isset($tag['name']) ? $tag['name'] : $tag;

            if ( ! Tag::whereName($tagName)->first()) {
                $newTag = new Tag();
                $newTag->name = $tagName;
                $newTag->save();
                $ids[] = $newTag->id;
            } else {
                $ids[] = $tag['id'];
            }
        }

        UserEdit::logManyToMany($address, 'rl_tags', $ids, $address->tags()->pluck('id')->toArray());

        $address->tags()->sync($ids);

        return response()->json($address);
    }

    /**
     * get all tags
     */
    public function loadAllTags($address)
    {
        $tags = Tag::all();

        return response()->json($tags);
    }

    /**
     * get selected tags for address
     */
    public function loadSelectedTags($address)
    {
        $address = Address::find($address);

        $selectedTags = $address->load('tags')->tags;

        return response()->json($selectedTags);
    }

    /**
     * get all clusters
     */
    public function getClusters()
    {
        $clusters = Cluster::get();
        return response()->json($clusters);
    }

    /**
     * update clusters
     */
    public function updateClusters($address)
    {
        $address = Address::find($address);

        $oldClusterId = $address->cluster_id;
        $address->cluster_id = request()->get('cluster_id');

        if(Auth::user()->default_country === 'usa-private') {
            $address->updated_by = 'labscape';
        }

        $address->update();
        $address->load('cluster');
        $address->load('cluster.addresses');
        $cluster = Cluster::with('addresses')
                    ->whereId($oldClusterId)
                    ->first();
        if ($cluster) {
            if ($cluster->addresses->count() < 1) {
                $cluster->delete();
            }
        }
        return response()->json($address);
    }

    /**
     * get all products
     */
    public function getProducts()
    {
        $products = Product::orderByRaw('company, name')->get();
        return response()->json($products);
    }

    /**
     * update used products for address
     */
    public function updateProducts($address)
    {
        $address = Address::find($address);

        $selectedProducts = request('selectedProducts');

        UserEdit::logManyToMany($address, 'rl_products', $selectedProducts, $address->products()->pluck('id')->toArray());

        $address->products()->sync($selectedProducts);

        $address->load([
            'products' => function ($query) {
                $query->orderByRaw('company, name');
            }
        ]);

        return response()->json($address);
    }

    /**
     * create new Product
     */
    public function createProduct ($address)
    {
        $address = Address::find($address);

        $company = trim(request('company'));
        $name = trim(request('name'));
        $description = trim(request('description'));

        if ( ! $company) {
            return response()->json([
                'status' => 'error',
                'message' => "Company field should not be empty"
            ]);
        }

        if ($company && ($name == "" || $name == null)) {
            $prod = Product::whereCompany($company)->where('name', '=', "")->orWhere('name', '=', null)->first();
            if ($prod) {
                return response()->json([
                    'status' => 'error',
                    'message' => "Product with company - '$company' already exists!"
                ]);
            }
        }

        if ($company && $name) {
            $prod = Product::whereCompany($company)->whereName($name)->first();
            if ($prod) {
                return response()->json([
                    'status' => 'error',
                    'message' => "This product already exists!"
                ]);
            }
        }

        if (strlen($company) > 255 || strlen($name) > 255) {
            return response()->json([
                'status' => 'error',
                'message' => "Max count of characters is 255!"
            ]);
        }

        $product = new Product();
        $product->company = $company;
        $product->name = $name;
        $product->description = $description;

        $image = request()->file('image');

        if ($image) {
            $extension = $image->getClientOriginalExtension();

            if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png') {
                $imageName = now()->format('Y-m-d-H-i-s') . '.' . $extension;

                $img = Image::make($image->getRealPath());

                $img->resize(100, 100, function ($constraint) {

                    $constraint->aspectRatio();

                })->save(storage_path("app/public/product-images/$imageName"));

                $product->image = "/product-images/$imageName";
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => "Only jpg/jpeg/png files are allowed!"
                ]);
            }
        }

        $product->save();

        $products = Product::orderByRaw('company, name')->get();

        $addressProduct = new AddressProduct();
        $addressProduct->address_id = $address->id;
        $addressProduct->product_id = $product->id;
        $addressProduct->save();

        $address->load([
            'products' => function ($query) {
                $query->orderByRaw('company, name');
            }
        ]);

        return response()->json([
            'products' => $products,
            'address' => $address
        ]);
    }

    /**
     * get all people for health system
     */
    public function getAllClusterStaff ($address)
    {
        $address = Address::find($address);

        $clusterStaff = People::with('addresses')
            ->whereHas('addresses', function ($q) use ($address) {
                $q->where('cluster_id', $address->cluster_id);
            })
            ->orderByRaw('name')
            ->get();

        return response()->json($clusterStaff);
    }

    /**
     * updating lab-chain name
     */
    public function updateClusterName ($address)
    {
        $address = Address::find($address);

        $cluster = Cluster::whereId($address->cluster->id)->first();
        $cluster->name = request('clusterName');
        $cluster->save();

        return response()->json($cluster);
    }

    /**
     * create new cluster if not exist
     */
    public function createCluster ($address)
    {
        $address = Address::find($address);

        $name = trim(request('name'));

        $cluster = Cluster::where('name', $name)->first();

        if ($cluster) {
            return response()->json([
                'status' => 'error',
                'message' => 'Health system already exists'
            ]);
        } else {
            $cluster = new Cluster();

            $cluster->name = $name;

            $cluster->save();

            $address->cluster_id = $cluster->id;

            $address->save();

            $cluster->load('addresses');

            return response()->json([
                'status' => 'success',
                'cluster' => $cluster
            ]);
        }
    }

    // create new relation from person to person
    public function createPersonRelation (Request $request)
    {
        $fromPersonId = request('fromPersonId');

        $toPersonId = request('toPersonId');

        $edgeType = request('edgeType');

        $edgeComment = request('edgeComment');

        $user = JWTAuth::user();

        $addressConnection = AddressConnection::where('from_person_id', $fromPersonId)
            ->where('to_person_id', $toPersonId)
            ->first();

        if ( ! empty($addressConnection)) {
            return response()->json([
                'success' => false,
                'message' => 'This connection already exists!'
            ]);
        }

        $addressConnection = new AddressConnection();

        $addressConnection->from_person_id = $fromPersonId;

        $addressConnection->from_address_id = null;

        $addressConnection->to_person_id = $toPersonId;

        $addressConnection->to_address_id = null;

        $addressConnection->edge_weight = 1;

        $addressConnection->edge_type = $edgeType;

        $addressConnection->edge_comment = $edgeComment;

        $addressConnection->edge_source = null;

        $addressConnection->user_id = $user->id;

        $addressConnection->save();

        return response()->json([
            'success' => true,
            'message' => 'ok'
        ]);
    }

    // delete relation from person to person
    public function deletePersonRelation (Request $request)
    {
        $fromPersonId = request('fromPersonId');

        $toPersonId = request('toPersonId');

        AddressConnection::where('from_person_id', $fromPersonId)
            ->where('to_person_id', $toPersonId)
            ->delete();

        return response()->json([
            'success' => true,
            'message' => 'ok'
        ]);
    }

    function composePaginatedResponseViaPureSql()
    {
        $total = $this->addressesService->countAddressesForUSA();

        $sql = $this->addressesService->prepareSqlForFetchAddressesWithoutGroupingByState(true);

        $sql .= $this->composeOrderByForAddressesWithoutGroupingByState(request()->all());

        $sql .= ' LIMIT 20' . ' OFFSET ' . $this->getOffsetForPagination();

        $result = $this->addressesService->getResultFromCache($sql);

        $data = [
            'total' => $total,
            'data' => $result
        ];

        return $data;
    }

    function getOffsetForPagination() {
        return 20 * (intval(request()->page) - 1);
    }


    function composeOrderByForAddressesWithoutGroupingByState($params)
    {
        $sql = '';

        if (isset($params['sort-by'])) {

            $field = explode('-',$params['sort-by'])[0];
            $direction = explode('-',$params['sort-by'])[1];

            if($field == 'people') {
                $field .= '_count';
            }
            else if($field == 'products') {
                $field .= '_count';
            }

            $sql = " ORDER BY $field $direction";
        }
        else {
            $sql = " ORDER BY people_count DESC, a.id DESC";
        }

        if (isset($params['iteration'])) {
            $sql = ' ORDER BY FIELD(a.id, '. implode(', ', $this->addressIdsDefinedWhenIteration).')';
        }

        return $sql;
    }


    function getStaffByRoleForChart($id)
    {
        $sql = "SELECT person_id, IF( IF(ap.role <> '' || ap.role IS NOT NUll, ap.role, p.role) = '', 'Others', ap.role) as role FROM rl_people as p
                JOIN rl_address_people as ap ON ap.person_id = p.id
                WHERE ap.address_id = $id
                ";

        $result = $this->addressesService->getResultFromCache($sql);

        $total = count($result);

        $rolesCount = [];

        foreach ($result as $record) {
            if(empty($rolesCount[$record->role])) {
                $rolesCount[$record->role] = 0;
            }

            ++$rolesCount[$record->role];
        }

        $groupedData = ['Others' => 0];

        foreach ($rolesCount as $role => $occur) {

            $rolesCount[$role] = [];

            $rolesCount[$role]['percentage'] = $occur / $total * 100;
            $rolesCount[$role]['occur'] = $occur;
        }

        foreach ($rolesCount as $role => $values) {

            if($role === 'Others' || $values['percentage'] < 3) {
                $groupedData['Others'] += $values['occur'];
            }
            else {
                $groupedData[$role] = $values['occur'];
            }
        }

        $graphData = [];

        foreach ($groupedData as $role => $occur) {
            $graphData[] = [$role, $occur];
        }

        array_unshift($graphData, ['Role', '%']);

        return response()->json($graphData);
    }


    function getAddressAwards($id)
    {
        $lrs = new LabscapeRocheService($this);

        return $lrs->getAddressAwards($id);
    }


    function getEmployeeRoleList($id)
    {
        $sql = "SELECT DISTINCT(IF(role <> '', role, '--empty--')) as role FROM rl_address_people WHERE address_id = ? ORDER BY role ASC";

        $result = $this->DBWithConnection()->select($sql, [$id]);

        $roleList = array_pluck($result, 'role');

        return response()->json($roleList);
    }


    function awardsDataForGaugeChart($id)
    {
        $address = Address::find($id);

        $graphMaxSql = 'SELECT ROUND(MAX(award_score), 1) as max_score FROM us_influencers_awards WHERE award_category_id = 4';
        $graphMax = $this->addressesService->getResultFromCache($graphMaxSql)[0]->max_score;

        $graphDataSql = "SELECT ROUND(MAX(award_score), 2) as award_score
                            FROM us_influencers_awards 
                            WHERE award_category_id = 4 
                            AND address_id = $address->id";

        $awardScore = $this->addressesService->getResultFromCache($graphDataSql)[0]->award_score;

        $graphData = ['Centrality', $awardScore];

        return response()->json([
            'data' => $graphData,
            'graphMax' => $graphMax
        ]);
    }


    function getDataForGrowthTrajectoryGauge($id)
    {
        $address = Address::find($id);

        $graphMaxSql = 'SELECT ROUND(MAX(score), 1) as max_score FROM us_expanders_events WHERE category_id = 4';
        $graphMax = $this->addressesService->getResultFromCache($graphMaxSql)[0]->max_score;

        $graphDataSql = "SELECT ROUND(MAX(score), 2) as score
                            FROM us_expanders_events 
                            WHERE category_id = 4 
                            AND address_id = $address->id";

        $awardScore = $this->addressesService->getResultFromCache($graphDataSql)[0]->score;

        $graphData = ['Growth', $awardScore];

        return response()->json([
            'data' => $graphData,
            'graphMax' => $graphMax
        ]);
    }

    function findCompanyByName()
    {
        $companyName = request()->get('name');

        $sql = "SELECT id, name, address FROM rl_addresses WHERE name LIKE ? LIMIT 50";

        $companies = $this->DBWithConnection()->select($sql, [$companyName.'%']);

        return response()->json($companies);
    }


    function assignAddressToUser()
    {
        $data = request()->validate([
            'userId' => 'required|numeric',
            'entityId' => 'required|numeric',
        ]);

        $address = Address::find($data['entityId']);
        $address->assigned_to = $data['userId'] == -1 ? NULL : $data['userId'];
        $address->save();

        return response()->json($address);
    }


    function loadUsaStates()
    {
        $user = Auth::user();

        if($user->default_country !== 'usa' && $user->default_country !== 'usa-private') {
            return response()->json([]);
        }

        $sql = "SELECT id, name, abv FROM rl_usa_state_coords";

        $states = $this->addressesService->getResultFromCache($sql);

        return response()->json($states);
    }

    public function share(Request $request, LabscapeRocheService $service)
    {
        if(!$service->isUserCanShareAddress(auth()->id())) {
            throw new \Exception('Too many emails sent per day');
        }
        $data = $request->validate([
            'to_name' => 'required|string',
            'address_id' => 'required|exists:rl_addresses,id',
            'to_email' => 'required|email',
            'to_subject' => 'required|string',
            'to_body' => 'required|string',
        ]);

        $service->shareAddress($data);
        return;
    }


    public function setAddressIgnore()
    {
        $data = \request()->validate([
            'reason' => 'required|numeric',
            'id' => 'required|numeric',
        ]);

        $address = Address::where('id', $data['id'])->first();
        $userId = Auth::user()->id;

        if($data['reason'] == 3) {

            $address->customer_status = 1;
            $address->save();

            return response()->json($address);
        }

        $addressIgnore = AddressIgnore::where('user_id', $userId)
            ->where('address_id', $address->id)
            ->where('ignored_type', $data['reason'])
            ->first();

        if(!$addressIgnore) {
            $addressIgnore = AddressIgnore::storeUserIgnore($userId, $address->id, $data['reason']);

            return response()->json($addressIgnore);
        }
        else {
            return response()->json(['message'=>'current ignore already exists'], 409);
        }
    }


    public function removeAddressFromIgnoreList()
    {
        $data = \request()->validate([
            'id' => 'required|numeric'
        ]);

        AddressIgnore::removeAddressFromIgnore($data['id']);

        return response()->json('ok');
    }
}




