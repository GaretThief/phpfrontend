<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

trait  USAAddressTrait
{
    public function fetchAddressesForUsaUser()
    {
        $foundAddressNumber = $this->countAddressesForUSA();
        if($foundAddressNumber > 10000 && !request()->get('state') && $this->isAuthUserDefaultCountry(Auth::user())) {
            $data = $this->findAddressesAndGroupByState();
            $result['data'] = $this->addAbbreviationProp($data);
            $result['groupedByState'] = true;
        } elseif($foundAddressNumber > 10000) {
            $result['data'] = $this->findAddressesAndGroupByLatLonTile();
            $result['groupedByState'] = true;
        }
        else {
            $result = $this->fetchAddressesWithoutGroupingByState();
        }

        return $result;
    }

    public function countAddressesForUSA()
    {

        $sql = "SELECT COUNT(DISTINCT a.id) as cnt FROM rl_addresses AS a";
        $sql .= $this->defineJoinsForUSA();
        $sql .= " WHERE a.lat <> 0 AND a.lon <> 0";
        $sql .= $this->composeConditionsUsa();
        $result = $this->DBWithConnection()->select(DB::raw($sql));

        return $result[0]->cnt;
    }

    public function defineJoinsForUSA($isForPagination = false)
    {
        $requestParams = request()->all();

        $mainSql = ' LEFT JOIN rl_address_precomputed_props as acomp ON acomp.address_id = a.id';
        $authUser = Auth::user();
        if($this->isAuthUserDefaultCountry($authUser)) {
            $mainSql .= ' LEFT JOIN rl_addresses_us_rms AS rms ON a.RSMf_SiteMasterId = rms.RSMf_SiteMasterId';

            if(isset($requestParams['scalar-types'])) {
                $mainSql .= GlobalHelper::addScalarValueJoins($requestParams);
            }

            if (isset($requestParams['zones'])
                || isset($requestParams['areas'])
                || isset($requestParams['franchises'])
                || isset($requestParams['tag-ids'])
            ) {
                $mainSql .= ' LEFT JOIN rl_addresses_us_rms_ext AS are ON a.RSMf_SiteMasterId = are.RSMf_SiteMasterId';
            }

            if(isset($requestParams['sort-by']) && $requestParams['sort-by'] === 'growthopp-desc') {
                $mainSql .= " LEFT JOIN rsm_scalar_values AS sv1 ON sv1.rsm_id = a.RSMf_SiteMasterId AND sv1.franchise = 'Enterprise' AND sv1.scalar_type_id = 10 ";
            }

            if(isset($requestParams['sort-by']) && $requestParams['sort-by'] === 'actualsales-desc') {
                $mainSql .= " LEFT JOIN rsm_scalar_values AS sv2 ON sv2.rsm_id = a.RSMf_SiteMasterId AND sv2.franchise = 'Enterprise' AND sv2.scalar_type_id = 11 ";
            }
        }

        if (!$this->isAuthUserDefaultCountry($authUser) && array_key_exists('scalar-types', $requestParams)) {
            $mainSql .= ' LEFT JOIN rl_scalar_values AS sv ON a.id = sv.address_id';
        }

        if ($isForPagination) {
            $mainSql .= ' LEFT JOIN rl_clusters AS cl ON a.cluster_id = cl.id';
        }

        if(isset($requestParams['tag-ids'])) {
            $mainSql .= " LEFT JOIN rl_address_tags AS at
                            ON at.address_id = a.id
                         LEFT JOIN rl_tags AS t
                            ON t.id = at.tag_id";
        }

        if(isset($requestParams['used-product-ids'])) {
            $mainSql .= " LEFT JOIN rl_address_products AS aprod
                            ON a.id = aprod.address_id
                         LEFT JOIN rl_products AS prod
                            ON prod.id = aprod.product_id";
        }

        return $mainSql;
    }

    public function isAuthUserDefaultCountry($user, string $defaultCountry = self::DEFAULT_COUNTRY)
    {
        return $user->default_country === self::DEFAULT_COUNTRY;
    }

    public function composeConditionsUsa()
    {
        $sql = '';
        $authUser = Auth::user();
        $requestParams = request();
        if (isset($requestParams['tag-ids']) && !$this->isAuthUserDefaultCountry($authUser)) {
            $sql .= ' AND t.id IN ('.implode(',', $requestParams['tag-ids']).')';
        } elseif (isset($requestParams['tag-ids']) && $this->isAuthUserDefaultCountry($authUser)) {
            $sql .= LabscapeRocheService::getAddressTagSqlCondition($requestParams['tag-ids']);
        }

        if (isset($requestParams['used-product-ids']) && !$this->isAuthUserDefaultCountry($authUser)) {
            $sql .= ' AND prod.id IN ('.implode(',', $requestParams['used-product-ids']).')';
        }

        if (isset($requestParams['used-product-ids']) && $this->isAuthUserDefaultCountry($authUser)) {
            $ids = LabscapeRocheService::selectCollectionOfAddressIdsByProducts($requestParams['used-product-ids'], $this);
            $idsStr = implode(',', $ids);
            $sql .= ' AND a.id IN ('.$idsStr.') ';
        }

        if (isset($requestParams['type-id'])) {
            if ($requestParams['type-id'] != -1) {
                $sql .= ' AND a.customer_status = ' . $requestParams['type-id'];
            } else {
                $ignoredAddressIds = AddressIgnore::getUserIgnoredAddressesIds();
                $ignoredAddressIdsStr = !empty($ignoredAddressIds) ? implode(',', $ignoredAddressIds) : -1;
                $sql .= " AND a.id IN ($ignoredAddressIdsStr)";
            }
        }

        if (isset($requestParams['global-search'])) {
            $sql .= ' AND a.name LIKE %'.$requestParams['global-search'].'%';
        }

        if (isset($requestParams['name'])) {
            $sql .= ' AND a.name LIKE %'.$requestParams['name'].'%';
        }

        if (isset($requestParams['iteration'])) {
            $addressIds = $this->getAddressIdsForIteration($requestParams);
            $this->addressIdsDefinedWhenIteration = $addressIds;
            $sql .= ' AND a.id IN('.implode(',', $addressIds).')';
        }

        if (isset($requestParams['tl_lat']) && isset($requestParams['tl_lon'])) {

            $p = $requestParams;

            $sql .= " AND ( a.lat < $p[tl_lat] AND a.lat > $p[br_lat] AND a.lon > $p[tl_lon] AND a.lon < $p[br_lon] )";
        }

        if (isset($requestParams['state'])) {
            $p = $requestParams;

            $sql .= " AND  rms.physical_state = '$p[state]'";
        }

        if (isset($requestParams['cluster-id'])) {
            $sql .= ' AND a.cluster_id = '.$requestParams['cluster-id'];
        }

        if (isset($requestParams['at-least-2-companies'])) {
            $clusterIds = (new Cluster)->getClusterIdsWithAtLeast2CompaniesInCluster();

            $sql .= ' AND a.cluster_id IN ('.implode(',',$clusterIds).') ';
        }

        if (isset($requestParams['is-favorite'])) {

            $favoriteIds = Favorite::getFavoriteEntityIds('ADDRESS');

            if(empty($favoriteIds)) {
                $sql .= ' AND a.id IN (-1) ';
            }
            else {
                $sql .= ' AND a.id IN ('.implode(',',$favoriteIds).') ';
            }

        }

        if (isset($requestParams['assignee'])) {
            $sql .= ' AND a.assigned_to IN ('.implode(',', $requestParams['assignee']).')';
        }

        if (isset($requestParams['market_programs'])) {

            $ids = LabscapeRocheService::selectCollectionOfIdsByMarketPrograms($requestParams['market_programs'], $this);

            $idsStr = implode(',', $ids);

            $sql .= ' AND a.id IN ('.$idsStr.')';
        }

        if (isset($requestParams['states'])) {

            $statesSql = "SELECT abv FROM rl_usa_state_coords WHERE id IN (".implode(',', $requestParams['states']).")";

            $statesAbbrev = array_pluck($this->DBWithConnection()->select($statesSql), 'abv');

            $statesAbbrev = array_map(function($el){return "'$el'";}, $statesAbbrev);

            $sql .= ' AND rms.physical_state IN ('.implode(',', $statesAbbrev).')';
        }

        if (isset($requestParams['baden-cities']) && env('APP_SCOPE') === 'wealthscape') {

            $citiesIds = $requestParams['baden-cities'];

            $addressesIds = $this->getAddressIdsInsideBadenRegion($citiesIds);

            if(is_array($citiesIds) && in_array(-2, $citiesIds)) {
                $addressesIds2 = $this->getAddressIdsOutsideBaden();
            }

            $addressesIds = array_merge($addressesIds, $addressesIds2 ?? []);

            if(empty($addressesIds)) {
                $addressesIds = [-1];
            }

            $sql .= ' AND a.id IN ('.implode(',', $addressesIds).') ';
        }

        if($this->isAuthUserDefaultCountry($authUser)) {
            $sql .= GlobalHelper::addUsaPrivateAddressesConditions($requestParams);
        }

        if (!$this->isAuthUserDefaultCountry($authUser) && array_key_exists('scalar-types', $requestParams)) {

            $addressIds = $this->selectCollectionOfAddressIdsByScalarTypes($requestParams['scalar-types']);

            $sql .= ' AND a.id IN ('.implode(',', $addressIds).')';
        }

        return $sql;
    }

    public function findAddressesAndGroupByState()
    {

        $subSubSql = $this->checkIfRequestedMin2CompaniesInCluster();

        $subSql = "SELECT rms.physical_state as state_abv, COUNT(DISTINCT $subSubSql) as total_addresses 
                FROM rl_addresses AS a";

        $subSql .= $this->defineJoinsForUSA();

        $subSql .= " WHERE a.lat <> 0 AND a.lon <> 0";

        $subSql .= $this->composeConditionsUsa();

        $subSql .= " GROUP BY rms.physical_state";

        $sql = "SELECT st.name, st.lon, st.lat, state_abv, total_addresses
            FROM ($subSql) subsql
            JOIN rl_usa_state_coords as st ON st.abv = state_abv
        ";

        return $this->getResultFromCache($sql);
    }

    protected function checkIfRequestedMin2CompaniesInCluster()
    {
        if(request()->get('at-least-2-companies')){
            $clusterIds = (new Cluster)->getClusterIdsWithAtLeast2CompaniesInCluster();

            $subSubSql = "(SELECT a0.id FROM rl_addresses AS a0 WHERE a.id = a0.id AND a0.cluster_id IN (".implode(',', $clusterIds)."))";
        }
        else {
            $subSubSql = 'a.id';
        }
        return $subSubSql;
    }

    public function getResultFromCache()
    {
        //todo
    }

    public function addAbbreviationProp($data) {

        foreach ($data as $value) {

            $count = $value->total_addresses;

            $abbrev = $count >= 10000 ? round($count / 1000) .'k' :
                $count >= 1000 ? round($count / 100) / 10  . 'k' : $count;


            $value->total_addresses_abbrev = (string)$abbrev;
        }

        return $data;
    }

    function findAddressesAndGroupByLatLonTile()
    {
        /**
         * TODO: this is the example of calculating the lat and lon for tile which values is "1000-116":
         * lat = 1000 * 0.05 = 50
         * lon = 116 * 0.106 = 12.29
         */


        $sql = "SELECT a.latlon_tile, a.name, COUNT(a.id) as total_addresses 
                FROM rl_addresses as a
                WHERE a.latlon_tile IS NOT NULL
                AND a.latlon_tile <> '0|0'
                GROUP BY a.latlon_tile";

        $result = $this->DBWithConnection()->select($sql);

        $data = [];

        foreach ($result as $i => $row) {

            $latLonArray = explode('|', $row->latlon_tile);

            $data[$i] = [];

            $data[$i]['name'] = $row->total_addresses > 1 ? $row->total_addresses : $row->name;
            $data[$i]['lat'] = $lat = intval($latLonArray[0]) * 0.05;
            $data[$i]['lon'] = $lon = intval($latLonArray[1]) * 0.106;
            $data[$i]['state_abv'] = $lat.';'.$lon;
            $data[$i]['total_addresses'] = $row->total_addresses;
            $data[$i]['total_addresses_abbrev'] = (string)$this->getAbbreviationValue($row->total_addresses);
        }

        return $data;
    }

    function getAbbreviationValue($count)
    {
        $abbrev = $count >= 10000 ? round($count / 1000) .'k' :
            $count >= 1000 ? round($count / 100) / 10  . 'k' : $count;

        return $abbrev;
    }

    public function fetchAddressesWithoutGroupingByState()
    {
        $sql = $this->prepareSqlForFetchAddressesWithoutGroupingByState();

        return $result = $this->getResultFromCache($sql);
    }

    function prepareSqlForFetchAddressesWithoutGroupingByState($isForPagination = false)
    {
        $sql = "SELECT a.id, a.name, a.lat, a.lon, a.customer_status, acomp.people_count";
        $authUser = Auth::user();
        if(env('APP_SCOPE') === 'wealthscape') {
            $sql .= ', a.assigned_to';
        }

        if ($isForPagination) {
            $sql .= ", a.address, cl.name as cluster_name";
        }

        if($this->isAuthUserDefaultCountry($authUser) && request()->get('sort-by') === 'growthopp-desc') {
            $sql .= ", sv1.scalar_value as growthopp";
        }

        if($this->isAuthUserDefaultCountry($authUser) && request()->get('sort-by') === 'actualsales-desc') {
            $sql .= ", sv2.scalar_value as actualsales";
        }

        $sql .= " FROM rl_addresses AS a";

        $sql .= $this->defineJoinsForUSA($isForPagination);

        $sql .= " WHERE a.lat <> 0 
                    AND a.lon <> 0 
                    AND a.lat IS NOT NULL 
                    AND a.lon IS NOT NULL";

        $sql .= $this->composeConditionsUsa();

        $sql .= ' GROUP BY a.id ';

        return $sql;
    }
}