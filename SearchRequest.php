<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'global-search' => 'required',
            'sort-by' => 'nullable',
            'tag-ids' => 'array|nullable',
            'used-product-ids' => 'array|nullable',
            'type-id' => 'integer|nullable',
            'name' => 'string|nullable',
            'iteration' => 'sometimes|array',
            'address-ids' => 'nullable',
            'cluster-id' => 'integer|nullable',
            'at-least-2-companies' => 'nullable',
        ];
    }
}