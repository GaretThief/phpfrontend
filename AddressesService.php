<?php

namespace App\Http\Controllers;

use App\Services\GlobalSearchService;
use App\Services\GlobalHelper;
use App\Models\Cluster;
use Illuminate\Database\Eloquent\Builder;

class AddressesService
{
    use USAAddressTrait;

    const DEFAULT_COUNTRY = 'usa-private';

    /**
     * @param SearchRequest $request
     * @return Builder
     */
    public function prepareAddressesQuery(SearchRequest $request): Builder
    {
        $query = $this->getAddressWithRelationsQuery();
        $query = $this->addFiltersToQuery($query, $request->all());
        return $query;
    }

    /**
     * @return Builder
     */
    public function getAddressWithRelationsQuery(): Builder
    {
        $query = Address::with(['tags', 'cluster'])
            ->withCount('people')
            ->with(['products' => function ($q) {
                $q
                    ->select('id')
                    ->orderByRaw('company, name');
            }]);
        return $query;
    }

    /**
     * @param $query
     * @param array $requestParams
     * @return Builder
     */
    public function addFiltersToQuery($query, array $requestParams): Builder
    {
        if (isset($requestParams['sort-by'])) {
            $sortBy = explode('-', $requestParams['sort-by']);
            $field = $sortBy[0];
            $direction = $sortBy[1];

            if($field == 'products') {
                $query->withCount('products');
            }
            $field .= '_count';
            $query->orderBy($field, $direction);
        }

        if (isset($requestParams['tag-ids'])) {
            $query->whereHas('tags', function ($q) use ($requestParams) {
                $q->whereIn('id', $requestParams['tag-ids']);
            });
        }

        if (isset($requestParams['used-product-ids'])) {
            $query->whereHas('products', function ($q) use ($requestParams) {
                $q->whereIn('id', $requestParams['used-product-ids']);
            });
        }

        if (isset($requestParams['type-id'])) {
            $query->whereHas('customerType', function ($q) use ($requestParams) {
                $q->where('id', $requestParams['type-id']);
            });
        }

        if (isset($requestParams['global-search'])) {
            $query->where('rl_addresses.name', 'LIKE', '%' . $requestParams['global-search'] . '%');
        }

        if (isset($requestParams['name'])) {
            $query->where('rl_addresses.name', 'LIKE', '%' . $requestParams['name'] . '%');
        }

        $addressIds = [];
        if (isset($requestParams['iteration'])) {
            $addressIds = $this->getAddressIdsForIteration($requestParams);
            $query->orderByRaw('FIELD(rl_addresses.id, '. implode(', ', $addressIds).')');
        }

        if (isset($requestParams['address-ids'])) {
            $addressIds = explode(',', $requestParams['address-ids']);
        }
        $query->whereIn('rl_addresses.id', $addressIds);


        if (isset($requestParams['cluster-id'])) {
            $query->where('cluster_id', $requestParams['cluster-id']);
        }

        if (isset($requestParams['at-least-2-companies'])) {
            $clusterIds = (new Cluster)->getClusterIdsWithAtLeast2CompaniesInCluster();
            $query->whereIn('rl_addresses.cluster_id', $clusterIds);
        }
        return $query;
    }

    /**
     * @param array $requestParams
     * @return mixed
     */
    public function getAddressIdsForIteration(array $requestParams)
    {
        $GSS = new GlobalSearchService();
        $groupedSearchIterations = $GSS->groupSearchIterationsByEntity($requestParams['iteration']);
        $isWithLevenstein = isset($requestParams['with-levenstein']);
        $addressIds = $GSS->searchForAddressesIds($groupedSearchIterations, $isWithLevenstein,  GlobalHelper::defineIsAddressesOrClustersShouldBeOrdered());
        return $addressIds;
    }

    public function DBWithConnection()
    {
        //todo
    }
}