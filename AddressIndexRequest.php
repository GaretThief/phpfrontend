<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;

class AddressIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'tag-ids' => 'array|nullable',
            'used-product-ids' => 'array|nullable',
            'type-id' => 'integer|nullable',
            'name' => 'string|nullable',
            'global-search' => 'string|nullable',
            'tl_lat' => 'numeric|nullable',
            'tl_lon' => 'numeric|nullable',
            'br_lat' => 'numeric|nullable',
            'br_lon' => 'numeric|nullable',
            'state' => 'string',
            'cluster-id' => 'integer|nullable',
            'assignee.*' => 'integer',
            'states.*' => 'integer',
            'zones' => 'array|nullable',
            'areas' => 'array|nullable',
            'baden-cities' => 'array|nullable',
            'is-favorite' => 'sometimes|required|numeric',
            'iteration' => 'sometimes|required|array',
            'scalar-types' => 'sometimes|required|array',
            'market_programs' => 'sometimes|required|array',
        ];
    }
}